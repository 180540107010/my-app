package com.aswdc.myapp.database;

import android.content.Context;

import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

public class MyDatabase extends SQLiteAssetHelper {

    public static final String DATABSE_NAME = "MyApp.db";
    public static final int DATABSE_VERSION = 1;

    public MyDatabase(Context context) {
        super(context, DATABSE_NAME, null, DATABSE_VERSION);
    }
}
