package com.aswdc.myapp;

import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.aswdc.myapp.database.MyDatabase;
import com.aswdc.myapp.database.TblUserData;

import java.util.ArrayList;
import java.util.HashMap;

public class MainActivity extends AppCompatActivity {

    EditText etName, etEmail, etPhoneNumber;

    Button btnSubmit;

    ImageView ivClose, ivClearName, ivClearEmail, ivClearPhoneNumber;

    RadioGroup rgGender;

    RadioButton rbMale, rbFemale;

    CheckBox chbCricket;
    CheckBox chbFootball;
    CheckBox chbHockey;

    ArrayList<HashMap<String, Object>> userList = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        init();
        setTypefaceOnView();
        clicks();
    }

        void clicks(){
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isValid()) {
                    String name = etName.getText().toString();
                    String email = etEmail.getText().toString();
                    String phoneNumber = etPhoneNumber.getText().toString();
                    String gender = rbMale.isChecked() ? rbMale.getText().toString() : rbFemale.getText().toString();
                    TblUserData tblUserData = new TblUserData(MainActivity.this);
                    long lastInsertedId = tblUserData.insertUserDetails(name, email, phoneNumber);
                    Toast.makeText(getApplicationContext(), lastInsertedId > 0 ? "User Inserted Successfully" : "Something went wrong", Toast.LENGTH_SHORT).show();
                }
            }
        });

            ivClose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    finish();
                }
            });

            ivClearName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    etName.getText().clear();
                }
            });

            ivClearEmail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    etEmail.getText().clear();
                }
            });

            ivClearPhoneNumber.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    etPhoneNumber.getText().clear();
                }
            });

            if (R.id.rbActFemale == rgGender.getCheckedRadioButtonId()) {
                chbHockey.setVisibility(View.VISIBLE);
                chbFootball.setVisibility(View.GONE);
                chbCricket.setVisibility(View.VISIBLE);
            }

            rgGender.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup radioGroup, int i) {
                    if (i == R.id.rbActMale) {
                        chbCricket.setVisibility(View.VISIBLE);
                        chbFootball.setVisibility(View.VISIBLE);
                        chbHockey.setVisibility(View.VISIBLE);
                    } else if (i == R.id.rbActFemale) {
                        chbCricket.setVisibility(View.VISIBLE);
                        chbFootball.setVisibility(View.GONE);
                        chbHockey.setVisibility(View.VISIBLE);
                    }
                }
            });
        }

    void init() {
        new MyDatabase(MainActivity.this).getReadableDatabase();
        etName = findViewById(R.id.etActName);
        etEmail = findViewById(R.id.etActEmail);
        etPhoneNumber = findViewById(R.id.etActPhoneNumber);
        btnSubmit = findViewById(R.id.btnActSubmit);
        ivClose = findViewById(R.id.ivActClose);
        ivClearName = findViewById(R.id.ivActClearName);
        ivClearEmail = findViewById(R.id.ivActClearEmail);
        ivClearPhoneNumber = findViewById(R.id.ivActClearPhoneNumber);

        rgGender = findViewById(R.id.rgActGender);
        rbMale = findViewById(R.id.rbActMale);
        rbFemale = findViewById(R.id.rbActFemale);

        chbCricket = findViewById(R.id.chbActCricket);
        chbFootball = findViewById(R.id.chbActFootball);
        chbHockey = findViewById(R.id.chbActHockey);

        etPhoneNumber.setText("+91");

        getSupportActionBar().setTitle(R.string.lbl_main_activity);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.dashboard_menu, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    void setTypefaceOnView() {
        Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/Gayathri-Thin.ttf");
        etName.setTypeface(typeface);
        etEmail.setTypeface(typeface);
        etPhoneNumber.setTypeface(typeface);
    }


    boolean isValid() {
        boolean flag = true;

        if (TextUtils.isEmpty(etName.getText())) {
            etName.setError(getString(R.string.error_enter_value));
            flag = false;
        } else {
            if (etName.getText().toString().contains(" ")) {
                etName.setError("No Spaces Allowed");
                flag = false;
            }
        }

        if(TextUtils.isEmpty(etEmail.getText())){
            etEmail.setError(getString(R.string.error_enter_value));
            flag = false;
        }
        else {
            String email = etEmail.getText().toString().trim();
            String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
            if (!email.matches(emailPattern)) {
                etEmail.setError(getString(R.string.error_invalid_email));
                flag = false;
            } else {
                if (etEmail.getText().toString().contains(" ")) {
                    etEmail.setError("No Spaces Allowed");
                    flag = false;
                }
            }
        }

        if(TextUtils.isEmpty(etPhoneNumber.getText())){
            etPhoneNumber.setError(getString(R.string.error_enter_value));
            flag = false;
        }
        else {
            if (etPhoneNumber.length() < 10) {
                etPhoneNumber.setError(getString(R.string.error_invalid_phone_number));
                flag = false;
            } else {
                if (etPhoneNumber.getText().toString().contains(" ")) {
                    etPhoneNumber.setError("No Spaces Allowed");
                    flag = false;
                }
            }
        }

        if (!chbCricket.isChecked()) {
            if (!chbFootball.isChecked()) {
                if (!chbHockey.isChecked()) {
                    Toast.makeText(getApplicationContext(), "Select one hobby", Toast.LENGTH_SHORT).show();
                    flag = false;
                }
            }
        }
        return flag;
    }

    boolean doubleBackToExitPressedOnce = false;
    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "click BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);

    }
}