package com.aswdc.myapp;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.aswdc.myapp.adapter.UserListAdapter;
import com.aswdc.myapp.util.Const;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

public class DisplayActivity extends AppCompatActivity {

    ListView lvUsers;
    Spinner spUsers;
    ArrayList<HashMap<String, Object>> userList = new ArrayList<>();
    UserListAdapter userListAdapter;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display);

        initViewReference();
        bindViewValues();
    }

    void initViewReference() {
        lvUsers = findViewById(R.id.lvActUsers);
        spUsers = findViewById(R.id.spActUsers);

        for (int i = 0; i < userList.size(); i++) {
            View view = LayoutInflater.from(DisplayActivity.this).inflate(R.layout.view_row_user_list, null);

            TextView tvName = view.findViewById(R.id.tvLstName);
            TextView tvEmail = view.findViewById(R.id.tvLstEmail);
            TextView tvPhoneNumber = view.findViewById(R.id.tvLstPhoneNumber);
            TextView tvGender = view.findViewById(R.id.tvLstGender);

            tvName.setText(userList.get(i).get(Const.NAME).toString());
            tvGender.setText(userList.get(i).get(Const.GENDER).toString());
            tvEmail.setText(String.valueOf(userList.get(i).get(Const.EMAIL)));

            lvUsers.addView(view);
        }

    }


    void bindViewValues() {
        userList.addAll((Collection<? extends HashMap<String, Object>>) getIntent().getSerializableExtra("UserList"));
        userListAdapter = new UserListAdapter(this, userList);
        lvUsers.setAdapter(userListAdapter);
        spUsers.setAdapter(userListAdapter);


        lvUsers.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                Toast.makeText(DisplayActivity.this, userList.get(spUsers.getSelectedItemPosition()).get(Const.NAME).toString(), Toast.LENGTH_SHORT).show();
            }
        });

        spUsers.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                Toast.makeText(DisplayActivity.this, userList.get(position).get(Const.GENDER).toString(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }
    
}
