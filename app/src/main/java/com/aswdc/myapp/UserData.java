package com.aswdc.myapp;

public class UserData {
    private String id;
    private String name;
    private String phoneNumber;
    private String email;

    public UserData(String id, String name, String phoneNumber, String email) {
        this.id = id;
        this.name = name;
        this.phoneNumber = phoneNumber;
        this.email = email;
    }

    public String getID() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public String getAge() {
        return this.phoneNumber;
    }

    public String getWeight() {
        return this.email;
    }
}