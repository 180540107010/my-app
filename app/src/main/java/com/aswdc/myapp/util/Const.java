package com.aswdc.myapp.util;

public class Const {
    public static final String NAME = "name";
    public static final String EMAIL = "email";
    public static final String PHONE_NUMBER = "phoneNumber";
    public static final String GENDER = "gender";
    public static final String HOBBY = "hobby";
}
